import { Config } from '../server/models/config';

export const productionConfig: Config = {
  serviceConfig: {
    name: 'Payment API Microservice',
    environment: 'production',
    namespace: 'payment-api',
    host: 'payment-api.prod.bharatride.in',
    description: 'Payment API to be used for microservices',
    port: 5555,
  },
  googleCloudConfig: {
    projectId: 'ride-api-prod',
    secrets: {
      database: 'database',
      firebase: 'firebase-service-account',
      directionapikey: 'direction-api',
      paytm: 'paytm-dev',
    },
    firestoreCollection: 'productionTimestamp',
  },
  endpoints: {
    userApi: 'https://user-api.prod.bharatride.in',
    deviceApi: 'https://device-api.prod.bharatride.in',
  },
};
