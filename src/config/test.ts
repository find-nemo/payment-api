import { Config } from '../server/models/config';

export const testConfig: Config = {
  serviceConfig: {
    name: 'Payment API Microservice',
    environment: 'test',
    namespace: 'payment-api',
    host: 'localhost',
    description: 'Payment API to be used for microservices',
    port: 5555,
  },
  googleCloudConfig: {
    projectId: 'rock-verbena-322706',
    secrets: {
      database: 'database',
      firebase: 'firebase-service-account',
      directionapikey: 'direction-api',
      paytm: 'paytm-dev',
    },
    firestoreCollection: 'stagingTimestamp',
  },
  endpoints: {
    userApi: 'https://user-api.stg.bharatride.in',
    deviceApi: 'https://device-api.stg.bharatride.in',
  },
};
