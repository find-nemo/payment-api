import { Router } from 'express';
import { PaytmInitTxnBody } from '../server/models/paytm';

import { appContainer } from '../inversify.config';
import { InterfaceRequestValidationMiddleware } from '../plugins/request-validation.interface';
import { HealthController } from '../server/controllers/health.controller';
import { PaymentController } from '../server/controllers/payment.controller';
import { SwaggerController } from '../server/controllers/swagger.controller';
import { Payment } from '../server/models/payment';

const routes = Router();

const requestValidationMiddleware =
  appContainer.get<InterfaceRequestValidationMiddleware>(
    nameof<InterfaceRequestValidationMiddleware>()
  );

const validationMiddleware =
  requestValidationMiddleware.validationMiddleware.bind(
    requestValidationMiddleware
  );

const swaggerController = appContainer.get<SwaggerController>(
  nameof<SwaggerController>()
);
const healthController = appContainer.get<HealthController>(
  nameof<HealthController>()
);
const paymentController = appContainer.get<PaymentController>(
  nameof<PaymentController>()
);

routes.get('/', healthController.getHealth.bind(healthController));

routes.get('/health', healthController.getHealth.bind(healthController));

routes.get('/api', swaggerController.getDocs.bind(swaggerController));

/**
 * @swagger
 * /payment/driver/{driverId}/:
 *  get:
 *    description: Get record of payment by driver Id
 *    summary: Returns list of payment by driver Id
 *    tags: [Payment]
 *    parameters:
 *      - name: driverId
 *        description: Driver Id
 *        in: path
 *        required: true
 *        schema:
 *          type: integer
 *      - name: limit
 *        description: Limit number of items to return
 *        in: query
 *        required: false
 *        schema:
 *          type: integer
 *      - name: offset
 *        description: Offset number of items skip
 *        in: query
 *        required: false
 *        schema:
 *          type: integer
 *    responses:
 *      200:
 *        description: Returns list of payment by driver Id
 *        content:
 *          application/json:
 *            schema:
 *              type: array
 *              items:
 *                $ref: '#/components/schemas/PaymentResponse'
 *      default:
 *       description: Error response
 *       content:
 *         application/json:
 *          schema:
 *            $ref: '#/components/schemas/ErrorResponse'
 */
routes.get(
  '/payment/driver/:driverId/',
  validationMiddleware('params', Payment, {
    validator: { groups: ['getByDriverId'] },
  }),
  paymentController.getByDriverId.bind(paymentController)
);

/**
 * @swagger
 * /payment/child/{childId}/:
 *  get:
 *    description: Get record of payment by child Id
 *    summary: Returns list of payment by child Id
 *    tags: [Payment]
 *    parameters:
 *      - name: childId
 *        description: Child Id
 *        in: path
 *        required: true
 *        schema:
 *          type: integer
 *      - name: limit
 *        description: Limit number of items to return
 *        in: query
 *        required: false
 *        schema:
 *          type: integer
 *      - name: offset
 *        description: Offset number of items skip
 *        in: query
 *        required: false
 *        schema:
 *          type: integer
 *    responses:
 *      200:
 *        description: Returns list of payment by child Id
 *        content:
 *          application/json:
 *            schema:
 *              type: array
 *              items:
 *                $ref: '#/components/schemas/PaymentResponse'
 *      default:
 *       description: Error response
 *       content:
 *         application/json:
 *          schema:
 *            $ref: '#/components/schemas/ErrorResponse'
 */
routes.get(
  '/payment/child/:childId/',
  validationMiddleware('params', Payment, {
    validator: { groups: ['getByChildId'] },
  }),
  paymentController.getByChildId.bind(paymentController)
);

/**
 * @swagger
 * /payment/:
 *  post:
 *    description: Record a new payment
 *    summary: Returns newly created payment record
 *    tags: [Payment]
 *    requestBody:
 *      description: Payment body
 *      required: true
 *      content:
 *       application/json:
 *        schema:
 *          $ref: '#/components/schemas/CreatePaymentBody'
 *    responses:
 *      200:
 *        description: Returns newly created payment record
 *        content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/schemas/PaymentResponse'
 *      default:
 *       description: Error response
 *       content:
 *         application/json:
 *          schema:
 *            $ref: '#/components/schemas/ErrorResponse'
 */
routes.post(
  '/payment',
  validationMiddleware('body', Payment, { validator: { groups: ['create'] } }),
  paymentController.create.bind(paymentController)
);

/**
 * @swagger
 * /payment/:
 *  put:
 *    description: Update the payment record
 *    summary: Returns updated payment record
 *    tags: [Payment]
 *    requestBody:
 *      description: Payment body
 *      required: true
 *      content:
 *       application/json:
 *        schema:
 *          $ref: '#/components/schemas/UpdatePaymentBody'
 *    responses:
 *      200:
 *        description: Returns updated payment record
 *        content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/schemas/PaymentResponse'
 *      default:
 *       description: Error response
 *       content:
 *         application/json:
 *          schema:
 *            $ref: '#/components/schemas/ErrorResponse'
 */
routes.put(
  '/payment',
  validationMiddleware('body', Payment, { validator: { groups: ['update'] } }),
  paymentController.update.bind(paymentController)
);

/**
 * @swagger
 * /payment/driver-confirm/id/{id}/:
 *  patch:
 *    description: Confirm the payment from driver
 *    summary: Returns the number of records updated
 *    tags: [Payment]
 *    parameters:
 *      - name: id
 *        description: id
 *        in: path
 *        required: true
 *        schema:
 *          type: integer
 *    responses:
 *      200:
 *        description: Returns the number of records updated
 *        content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/schemas/UpdateRecordsResponse'
 *      default:
 *       description: Error response
 *       content:
 *         application/json:
 *          schema:
 *            $ref: '#/components/schemas/ErrorResponse'
 */
routes.patch(
  '/payment/driver-confirm/id/:id/',
  validationMiddleware('params', Payment, {
    validator: { groups: ['confirmDriverTransaction'] },
  }),
  paymentController.confirmDriverPayment.bind(paymentController)
);

/**
 * @swagger
 * /payment/parent-confirm/id/{id}/:
 *  patch:
 *    description: Confirm the payment from parent
 *    summary: Returns the number of records updated
 *    tags: [Payment]
 *    parameters:
 *      - name: id
 *        description: id
 *        in: path
 *        required: true
 *        schema:
 *          type: integer
 *    responses:
 *      200:
 *        description: Returns the number of records updated
 *        content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/schemas/UpdateRecordsResponse'
 *      default:
 *       description: Error response
 *       content:
 *         application/json:
 *          schema:
 *            $ref: '#/components/schemas/ErrorResponse'
 */
routes.patch(
  '/payment/parent-confirm/id/:id/',
  validationMiddleware('params', Payment, {
    validator: { groups: ['confirmParentTransaction'] },
  }),
  paymentController.confirmParentPayment.bind(paymentController)
);

/**
 * @swagger
 * /payment/:
 *  delete:
 *    description: Delete the payment record
 *    summary: Returns deleted payment record
 *    tags: [Payment]
 *    parameters:
 *      - name: uId
 *        description: UId
 *        in: query
 *        required: true
 *        schema:
 *          type: string
 *    responses:
 *      200:
 *        description: Returns deleted payment record
 *        content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/schemas/PaymentResponse'
 *      default:
 *       description: Error response
 *       content:
 *         application/json:
 *          schema:
 *            $ref: '#/components/schemas/ErrorResponse'
 */
routes.delete(
  '/payment/',
  validationMiddleware('params', Payment, {
    validator: { groups: ['delete'] },
  }),
  paymentController.delete.bind(paymentController)
);

/**
 * @swagger
 * /paytm/initTxn:
 *  post:
 *    description: Initiate Paytm Transaction
 *    summary: Returns transaction token to be used for completing payment
 *    tags: [Paytm]
 *    requestBody:
 *      description: Transaction body
 *      required: true
 *      content:
 *       application/json:
 *        schema:
 *          $ref: '#/components/schemas/CreateTransactionBody'
 *    responses:
 *      200:
 *        description: Returns transaction record
 *        content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/schemas/TransactionResponse'
 *      default:
 *       description: Error response
 *       content:
 *         application/json:
 *          schema:
 *            $ref: '#/components/schemas/ErrorResponse'
 */
routes.post(
  '/paytm/initTxn',
  validationMiddleware('body', PaytmInitTxnBody, {
    validator: { groups: ['initTxn'] },
  }),
  paymentController.initTxn.bind(paymentController)
);

export { routes };
