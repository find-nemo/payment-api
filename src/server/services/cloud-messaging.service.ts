import * as admin from "firebase-admin";
import { inject, injectable } from "inversify";
import _ from "lodash";
import { logger } from "../utils/logger";
import { CloudMessagingInterface } from "./cloud-messaging.interface";
import { ChildDataAccessInterface } from "../repository/child.interface";
import { Payment } from "../models/payment";
import { DeviceDataAccessInterface } from "../repository/device.interface";
import { UserDataAccessInterface } from "../repository/user.interface";

@injectable()
export class CloudMessagingService implements CloudMessagingInterface {
  constructor(
    @inject(nameof<ChildDataAccessInterface>())
    private _childDataAccess: ChildDataAccessInterface,
    @inject(nameof<DeviceDataAccessInterface>())
    private _deviceDataAccess: DeviceDataAccessInterface,
    @inject(nameof<UserDataAccessInterface>())
    private _userDataAccess: UserDataAccessInterface
  ) {}

  async driverRecordPaymentMessage(payment: Payment): Promise<void> {
    try {
      const parent = await this._childDataAccess.getChildParent(
        payment.childId
      );
      const driver = await this._userDataAccess.getUserById(payment.driverId);
      if (!parent) {
        logger.error("No parent found for the child", {
          payment,
        });
        return;
      }

      const devices = await this._deviceDataAccess.getByUserId(parent.id);

      const message: admin.messaging.MulticastMessage = {
        android: {
          notification: {
            clickAction: "FLUTTER_NOTIFICATION_CLICK",
          },
        },
        notification: {
          title: `${driver?.fullName} recorded payment for Rs. ${payment.amount}`,
          body: `View and confirm payment details`,
        },
        data: {
          type: "payment",
          id: `${payment.id}`,
          childId: `${payment.childId}`,
        },
        tokens: [],
      };

      if (devices && devices.length) {
        for (const d of _.chunk(devices, 500)) {
          message.tokens = [];
          if (d && d.length) {
            d.forEach((device) => {
              const fcmToken = device.getDataValue("fcmToken");
              if (fcmToken) {
                message.tokens.push(fcmToken);
              }
            });
          }
          if (message && message.tokens && message.tokens.length) {
            await admin.messaging().sendMulticast(message);
          }
        }
      }
    } catch (error) {
      logger.error(
        "Error while sending push notification to the parent devices",
        { payment, error }
      );
    }
  }
}
