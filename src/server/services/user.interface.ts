import { User } from '../models/user';

export interface UserServiceInterface {
  getUserByPhoneNumber(phoneNumber: string): Promise<User | null>;
}
