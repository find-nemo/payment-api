import { inject, injectable } from 'inversify';
import { Payment } from '../models/payment';
import { PaytmInitTxnResponse, PaytmSecret } from '../models/paytm';
import { PaymentDataAccessInterface } from '../repository/payment.interface';
import { PaymentServiceInterface } from './payment.interface';
import axios from 'axios';
import { InterfaceSecretManagerPlugin } from '../../plugins/secret-manager.interface';
import { SecretKeys } from '../models/secrets';

const PaytmChecksum = require('paytmchecksum');

@injectable()
export class PaymentService implements PaymentServiceInterface {
  constructor(
    @inject(nameof<PaymentDataAccessInterface>())
    private _paymentDataAccess: PaymentDataAccessInterface,
    @inject(nameof<InterfaceSecretManagerPlugin>())
    private _secretManagerPlugin: InterfaceSecretManagerPlugin
  ) {}

  async initTxn(
    userId: number,
    amount: string,
    orderId: string
  ): Promise<PaytmInitTxnResponse> {
    const paytmSecret: PaytmSecret =
      await this._secretManagerPlugin.getSecretJson(SecretKeys.paytm);
    const data = {
      requestType: 'Payment',
      mid: paytmSecret.mid,
      websiteName: paytmSecret.websiteName,
      orderId: orderId,
      callbackUrl: `https://securegw-stage.paytm.in/theia/paytmCallback?ORDER_ID=${orderId}`,
      txnAmount: {
        value: amount,
        currency: 'INR',
      },
      userInfo: {
        custId: userId,
      },
    };

    const checksum = await PaytmChecksum.generateSignature(
      JSON.stringify(data),
      paytmSecret.mkey
    );

    let paytmParams = {
      head: {
        signature: checksum,
      },
      body: data,
    };

    let post_data = JSON.stringify(paytmParams);

    const url = `https://${paytmSecret.hostname}/theia/api/v1/initiateTransaction?mid=${paytmSecret.mid}&orderId=${orderId}`;

    const response = await axios.post(url, post_data, {
      headers: {
        'Content-Type': 'application/json',
        'Content-Length': post_data.length,
      },
    });

    if (response.data.body.txnToken) {
      return { txnToken: response.data.body.txnToken };
    } else {
      throw response.data.body;
    }
  }

  async getByUId(
    uId: string,
    limit?: number,
    offset?: number
  ): Promise<Payment[] | null> {
    return this._paymentDataAccess.getByUId(uId, limit, offset);
  }

  async getOneByUId(uId: string): Promise<Payment | null> {
    return this._paymentDataAccess.getOneByUId(uId);
  }

  async getByDriverId(
    driverId: number,
    limit: number,
    offset: number
  ): Promise<Payment[] | undefined> {
    return this._paymentDataAccess.getByDriverId(driverId, limit, offset);
  }

  async getByChildId(
    childId: number,
    limit: number,
    offset: number
  ): Promise<Payment[] | undefined> {
    return this._paymentDataAccess.getByChildId(childId, limit, offset);
  }

  async create(payment: Payment): Promise<Payment> {
    return this._paymentDataAccess.create(payment);
  }

  async confirmDriverPayment(id: number): Promise<[number]> {
    return this._paymentDataAccess.confirmDriverPayment(id);
  }
  async confirmParentPayment(id: number): Promise<[number]> {
    return this._paymentDataAccess.confirmParentPayment(id);
  }
}
