import { Payment } from "../models/payment";
export interface CloudMessagingInterface {
  driverRecordPaymentMessage(payment: Payment): Promise<void>;
}
