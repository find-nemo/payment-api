import { injectable, inject } from 'inversify';
import { UserServiceInterface } from './user.interface';
import { UserDataAccessInterface } from '../repository/user.interface';
import { User } from '../models/user';

@injectable()
export class UserService implements UserServiceInterface {
  constructor(
    @inject(nameof<UserDataAccessInterface>())
    private _userDataAccess: UserDataAccessInterface
  ) {}

  async getUserByPhoneNumber(phoneNumber: string): Promise<User | null> {
    return this._userDataAccess.getUserByPhoneNumber(phoneNumber);
  }
}
