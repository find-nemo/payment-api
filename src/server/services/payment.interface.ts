import { Payment } from '../models/payment';
import { PaytmInitTxnResponse } from '../models/paytm';

export interface PaymentServiceInterface {
  getByUId(
    uId: string,
    limit?: number,
    offset?: number
  ): Promise<Payment[] | null>;
  getOneByUId(uId: string): Promise<Payment | null>;
  getByDriverId(
    driverId: number,
    limit: number,
    offset: number
  ): Promise<Payment[] | undefined>;
  getByChildId(
    childId: number,
    limit: number,
    offset: number
  ): Promise<Payment[] | undefined>;
  create(payment: Payment): Promise<Payment>;
  confirmDriverPayment(id: number): Promise<[number]>;
  confirmParentPayment(id: number): Promise<[number]>;
  initTxn(
    userId: number,
    amount: string,
    orderId: string
  ): Promise<PaytmInitTxnResponse>;
}
