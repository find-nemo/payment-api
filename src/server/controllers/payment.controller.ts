import { NextFunction, Request, Response } from 'express';
import { inject, injectable } from 'inversify';
import moment from 'moment';
import { v4 as uuidv4 } from 'uuid';
import { InterfaceErrorHandlerPlugin } from '../../plugins/error-handler.interface';
import { Payment } from '../models/payment';
import { PaymentStatusEnum } from '../models/payment-status-enum';
import {
  RequestWithBody,
  RequestWithParams,
  RequestWithPhoneNumber,
} from '../models/request-validation';
import { PaymentServiceInterface } from '../services/payment.interface';
import { UserServiceInterface } from '../services/user.interface';
import { CloudMessagingInterface } from '../services/cloud-messaging.interface';
import { PaytmInitTxnBody } from '../models/paytm';

@injectable()
export class PaymentController {
  private _error: InterfaceErrorHandlerPlugin;
  constructor(
    @inject(nameof<InterfaceErrorHandlerPlugin>())
    errorFactory: (name: string) => InterfaceErrorHandlerPlugin,
    @inject(nameof<UserServiceInterface>())
    private _userService: UserServiceInterface,
    @inject(nameof<PaymentServiceInterface>())
    private _paymentService: PaymentServiceInterface,
    @inject(nameof<CloudMessagingInterface>())
    private _cloudMessaging: CloudMessagingInterface
  ) {
    this._error = errorFactory(nameof<PaymentController>());
  }

  async getByDriverId(
    req: Request,
    res: Response,
    next: NextFunction
  ): Promise<any> {
    const request = req as RequestWithParams<Payment>;
    res.json(
      await this._paymentService.getByDriverId(
        request.validatedParams.driverId,
        request.validatedParams.limit ?? 20,
        request.validatedParams.offset ?? 0
      )
    );
    next();
  }

  async getByChildId(
    req: Request,
    res: Response,
    next: NextFunction
  ): Promise<any> {
    const request = req as RequestWithParams<Payment>;
    res.json(
      await this._paymentService.getByChildId(
        request.validatedParams.childId,
        request.validatedParams.limit ?? 20,
        request.validatedParams.offset ?? 0
      )
    );
    next();
  }

  async create(req: Request, res: Response, next: NextFunction): Promise<any> {
    const request = req as RequestWithBody<Payment> & RequestWithPhoneNumber;
    this.validateDates(request.validatedBody.dates);

    const user = await this._userService.getUserByPhoneNumber(
      request.validatedPhoneNumber
    );

    if (!user) {
      throw this._error.getFormattedError({
        source: 'extapi',
        status: 400,
        message: 'Cannot find user by phoneNumber',
        errorData: {
          phoneNumber: request.validatedPhoneNumber,
        },
      });
    }

    const payment = request.validatedBody;
    payment.creatorId = user.id;
    payment.uId = uuidv4();
    payment.createdAt = moment.utc().toDate();
    payment.status = PaymentStatusEnum.CREATED;
    if (payment.creatorId == payment.driverId) {
      payment.hasDriverAccepted = true;
      payment.hasParentAccepted = false;
    } else {
      payment.hasParentAccepted = true;
      payment.hasDriverAccepted = false;
    }
    const data = await this._paymentService.create(payment);
    data.amount = <any>data.amount.toString();
    res.json(data);
    await this._cloudMessaging.driverRecordPaymentMessage(data);
    next();
  }

  private validateDates(dates: string): void {
    const re =
      /^((1[0-2]|0[1-9]|\d)\/([2-9]\d[1-9]\d|[1-9]\d),)*((1[0-2]|0[1-9]|\d)\/([2-9]\d[1-9]\d|[1-9]\d)){1}$/gi;
    if (!re.test(dates)) {
      throw this._error.getFormattedError({
        status: 400,
        message: 'Error while validating dates',
        source: 'int',
        errorData: {
          dates,
        },
      });
    }
  }

  async update(req: Request, res: Response, next: NextFunction): Promise<any> {
    const request = req as RequestWithBody<Payment> & RequestWithPhoneNumber;
    if (request.validatedBody.dates) {
      this.validateDates(request.validatedBody.dates);
    }

    const user = await this._userService.getUserByPhoneNumber(
      request.validatedPhoneNumber
    );

    if (!user) {
      throw this._error.getFormattedError({
        source: 'extapi',
        status: 400,
        message: 'Cannot find user by phoneNumber',
        errorData: {
          phoneNumber: request.validatedPhoneNumber,
        },
      });
    }

    const oldPayment = await this._paymentService.getOneByUId(
      request.validatedBody.uId
    );

    if (!oldPayment) {
      throw this._error.getFormattedError({
        source: 'intdb',
        status: 400,
        message: 'Cannot find payment by uId',
        errorData: {
          uId: request.validatedBody.uId,
        },
      });
    }

    const payment = new Payment();
    payment.creatorId = user.id;
    payment.status = PaymentStatusEnum.UPDATED;
    payment.uId = oldPayment.uId;
    payment.driverId = oldPayment.driverId;
    payment.childId = oldPayment.childId;
    payment.amount = request.validatedBody.amount ?? oldPayment.amount;
    payment.notes = request.validatedBody.notes ?? oldPayment.notes;
    payment.dates = request.validatedBody.dates ?? oldPayment.dates;
    payment.createdAt = moment.utc().toDate();
    if (payment.creatorId == payment.driverId) {
      payment.hasDriverAccepted = true;
      payment.hasParentAccepted = false;
    } else {
      payment.hasParentAccepted = true;
      payment.hasDriverAccepted = false;
    }
    const data = await this._paymentService.create(payment);
    data.amount = <any>data.amount.toString();
    res.json(data);
    await this._cloudMessaging.driverRecordPaymentMessage(data);
    next();
  }

  async confirmDriverPayment(
    req: Request,
    res: Response,
    next: NextFunction
  ): Promise<any> {
    const request = req as RequestWithParams<Payment>;
    res.json({
      updated: (
        await this._paymentService.confirmDriverPayment(
          request.validatedParams.id
        )
      )[0],
    });
    next();
  }

  async confirmParentPayment(
    req: Request,
    res: Response,
    next: NextFunction
  ): Promise<any> {
    const request = req as RequestWithParams<Payment>;
    res.json({
      updated: (
        await this._paymentService.confirmParentPayment(
          request.validatedParams.id
        )
      )[0],
    });
    next();
  }

  async delete(req: Request, res: Response, next: NextFunction): Promise<any> {
    const request = req as RequestWithParams<Payment> & RequestWithPhoneNumber;

    const user = await this._userService.getUserByPhoneNumber(
      request.validatedPhoneNumber
    );

    if (!user) {
      throw this._error.getFormattedError({
        source: 'extapi',
        status: 400,
        message: 'Cannot find user by phoneNumber',
        errorData: {
          phoneNumber: request.validatedPhoneNumber,
        },
      });
    }

    const oldPayment = await this._paymentService.getOneByUId(
      request.validatedParams.uId
    );

    if (!oldPayment) {
      throw this._error.getFormattedError({
        source: 'intdb',
        status: 400,
        message: 'Cannot find payment by uId',
        errorData: {
          uId: request.validatedParams.uId,
        },
      });
    }

    const payment = new Payment();
    payment.creatorId = user.id;
    payment.status = PaymentStatusEnum.DELETED;
    payment.uId = oldPayment.uId;
    payment.driverId = oldPayment.driverId;
    payment.childId = oldPayment.childId;
    payment.amount = oldPayment.amount;
    payment.notes = oldPayment.notes;
    payment.dates = oldPayment.dates;
    payment.hasDriverAccepted = oldPayment.hasDriverAccepted;
    payment.hasParentAccepted = oldPayment.hasParentAccepted;
    payment.createdAt = moment.utc().toDate();
    const data = await this._paymentService.create(payment);
    data.amount = <any>data.amount.toString();
    res.json(data);
    next();
  }

  async initTxn(req: Request, res: Response, next: NextFunction): Promise<any> {
    const request = req as RequestWithBody<PaytmInitTxnBody> &
      RequestWithPhoneNumber;
    const user = await this._userService.getUserByPhoneNumber(
      request.validatedPhoneNumber
    );

    if (!user) {
      throw this._error.getFormattedError({
        source: 'extapi',
        status: 400,
        message: 'Cannot find user by phoneNumber',
        errorData: {
          phoneNumber: request.validatedPhoneNumber,
        },
      });
    }

    try {
      const response = await this._paymentService.initTxn(
        user.id,
        request.validatedBody.amount,
        request.validatedBody.orderId
      );
      res.json(response);
    } catch (e) {
      throw this._error.getFormattedError({
        source: 'extapi',
        status: 400,
        message: 'Error while getting txn token',
        errorData: { body: e },
      });
    }

    next();
  }
}
