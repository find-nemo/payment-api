import { User } from "../models/user";

export interface UserDataAccessInterface {
  getUserByPhoneNumber(phoneNumber: string): Promise<User | null>;
  getUserById(userId: number): Promise<User | null>;
}
