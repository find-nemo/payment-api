import { inject, injectable } from 'inversify';
import { InterfaceSequelizePlugin } from '../../plugins/sequelize.interface';
import { InterfaceErrorHandlerPlugin } from '../../plugins/error-handler.interface';
import { Payment } from '../models/payment';
import { PaymentDataAccessInterface } from './payment.interface';

@injectable()
export class PaymentDataAccess implements PaymentDataAccessInterface {
  private _error: InterfaceErrorHandlerPlugin;

  constructor(
    @inject(nameof<InterfaceErrorHandlerPlugin>())
    errorFactory: (name: string) => InterfaceErrorHandlerPlugin,
    @inject(nameof<InterfaceSequelizePlugin>()) private _sequelize: InterfaceSequelizePlugin
  ) {
    this._error = errorFactory(nameof<PaymentDataAccess>());
  }

  async getByUId(
    uId: string,
    limit?: number,
    offset?: number
  ): Promise<Payment[] | null> {
    try {
      return await Payment.findAll({
        where: { uId },
        limit,
        offset,
        order: [
          ['created_at', 'DESC'],
          ['id', 'DESC'],
        ],
      });
    } catch (error) {
      throw this._error.getFormattedError({
        status: 500,
        message: 'Error while getting payment by driverId',
        source: 'intdb',
        errorData: {
          error,
          uId,
          limit,
          offset,
        },
      });
    }
  }

  async getOneByUId(uId: string): Promise<Payment | null> {
    try {
      return await Payment.findOne({
        where: { uId },
        order: [
          ['created_at', 'DESC'],
          ['id', 'DESC'],
        ],
      });
    } catch (error) {
      throw this._error.getFormattedError({
        status: 500,
        message: 'Error while getting payment by driverId',
        source: 'intdb',
        errorData: {
          error,
          uId,
        },
      });
    }
  }

  async getByDriverId(
    driverId: number,
    limit: number,
    offset: number
  ): Promise<Payment[] | undefined> {
    try {
      if (!this._sequelize.getSequelize()) {
        throw Error('Sequelize object not found.')
      }

      const query = `select payment.id,
      payment.uId,
      payment.created_at          as createdAt,
      payment.child_id            as childId,
      payment.driver_id           as driverId,
      payment.amount,
      payment.creator_id          as creatorId,
      payment.dates,
      payment.has_driver_accepted as hasDriverAccepted,
      payment.has_parent_accepted as hasParentAccepted,
      payment.notes,
      payment.status
from payment,
    (select uId, max(created_at) as createdAt
     from payment
     group by uId
     order by createdAt desc) max_user
where payment.uId = max_user.uId
 and driver_id = ${driverId}
 and payment.created_at = max_user.createdAt
order by created_at desc limit ${limit} offset ${offset};`;
      return await this._sequelize.getSequelize()?.query(query, {type: 'SELECT', model: Payment});
    } catch (error) {
      throw this._error.getFormattedError({
        status: 500,
        message: 'Error while getting payment by driverId',
        source: 'intdb',
        errorData: {
          error,
          driverId,
          limit,
          offset,
        },
      });
    }
  }

  async getByChildId(
    childId: number,
    limit: number,
    offset: number
  ): Promise<Payment[] | undefined> {
    try {
      if (!this._sequelize.getSequelize()) {
        throw Error('Sequelize object not found.')
      }

      const query = `select payment.id,
      payment.uId,
      payment.created_at          as createdAt,
      payment.child_id            as childId,
      payment.driver_id           as driverId,
      payment.amount,
      payment.creator_id          as creatorId,
      payment.dates,
      payment.has_driver_accepted as hasDriverAccepted,
      payment.has_parent_accepted as hasParentAccepted,
      payment.notes,
      payment.status
from payment,
    (select uId, max(created_at) as createdAt
     from payment
     group by uId
     order by createdAt desc) max_user
where payment.uId = max_user.uId
 and child_id = ${childId}
 and payment.created_at = max_user.createdAt
order by created_at desc limit ${limit} offset ${offset};`;
      return await this._sequelize.getSequelize()?.query(query, {type: 'SELECT', model: Payment});
    } catch (error) {
      throw this._error.getFormattedError({
        status: 500,
        message: 'Error while getting payment by childId',
        source: 'intdb',
        errorData: {
          error,
          childId,
          limit,
          offset,
        },
      });
    }
  }

  async create(payment: Payment): Promise<Payment> {
    try {
      if (payment.save) {
        return await payment.save();
      }
      return await Payment.create(payment);
    } catch (error) {
      throw this._error.getFormattedError({
        status: 500,
        message: 'Error while creating payment',
        source: 'intdb',
        errorData: {
          error,
          payment,
        },
      });
    }
  }

  async confirmDriverPayment(id: number): Promise<[number]> {
    try {
      return await Payment.update(
        {
          hasDriverAccepted: true,
        },
        { where: { id } }
      );
    } catch (error) {
      throw this._error.getFormattedError({
        status: 500,
        message: 'Error while confirming payment for driver',
        source: 'intdb',
        errorData: {
          error,
          id,
        },
      });
    }
  }

  async confirmParentPayment(id: number): Promise<[number]> {
    try {
      return await Payment.update(
        {
          hasParentAccepted: true,
        },
        { where: { id } }
      );
    } catch (error) {
      throw this._error.getFormattedError({
        status: 500,
        message: 'Error while confirming payment for parent',
        source: 'intdb',
        errorData: {
          error,
          id,
        },
      });
    }
  }
}
