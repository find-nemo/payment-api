import { Payment } from '../models/payment';

export interface PaymentDataAccessInterface {
  getByUId(
    uId: string,
    limit?: number,
    offset?: number
  ): Promise<Payment[] | null>;
  getOneByUId(uId: string): Promise<Payment | null>;
  getByDriverId(
    driverId: number,
    limit: number,
    offset: number
  ): Promise<Payment[] | undefined>;
  getByChildId(
    childId: number,
    limit: number,
    offset: number
  ): Promise<Payment[] | undefined>;
  create(payment: Payment): Promise<Payment>;
  confirmDriverPayment(id: number): Promise<[number]>;
  confirmParentPayment(id: number): Promise<[number]>;
}
