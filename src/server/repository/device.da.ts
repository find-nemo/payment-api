import { inject, injectable } from "inversify";
import { Op } from "sequelize";

import { InterfaceErrorHandlerPlugin } from "../../plugins/error-handler.interface";
import { Device } from "../models/device";
import { DeviceDataAccessInterface } from "./device.interface";

@injectable()
export class DeviceDataAccess implements DeviceDataAccessInterface {
  private _error: InterfaceErrorHandlerPlugin;

  constructor(
    @inject(nameof<InterfaceErrorHandlerPlugin>())
    errorFactory: (name: string) => InterfaceErrorHandlerPlugin
  ) {
    this._error = errorFactory(nameof<DeviceDataAccess>());
  }

  async getByUserId(userId: number): Promise<Device[] | null> {
    try {
      return await Device.findAll({
        where: {
          userId,
          fcmToken: { [Op.not]: null as any },
        },
      });
    } catch (error) {
      throw this._error.getFormattedError({
        status: 500,
        message: "Error while getting devices by userId",
        source: "intdb",
        errorData: {
          error,
          userId,
        },
      });
    }
  }
}
