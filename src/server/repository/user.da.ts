import { injectable, inject } from "inversify";
import { UserDataAccessInterface } from "./user.interface";
import { InterfaceErrorHandlerPlugin } from "../../plugins/error-handler.interface";
import { User } from "../models/user";

@injectable()
export class UserDataAccess implements UserDataAccessInterface {
  private _error: InterfaceErrorHandlerPlugin;

  constructor(
    @inject(nameof<InterfaceErrorHandlerPlugin>())
    errorFactory: (name: string) => InterfaceErrorHandlerPlugin
  ) {
    this._error = errorFactory(nameof<UserDataAccess>());
  }

  async getUserByPhoneNumber(phoneNumber: string): Promise<User | null> {
    try {
      return await User.findOne({
        where: {
          phoneNumber,
        },
      });
    } catch (error) {
      throw this._error.getFormattedError({
        status: 500,
        message: "Error while getting user by phoneNumber",
        source: "intdb",
        errorData: {
          error,
          phoneNumber,
        },
      });
    }
  }

  async getUserById(userId: number): Promise<User | null> {
    try {
      return await User.findOne({
        where: {
          id: userId,
        },
      });
    } catch (error) {
      throw this._error.getFormattedError({
        status: 500,
        message: "Error while getting user by id",
        source: "intdb",
        errorData: {
          error,
          userId,
        },
      });
    }
  }
}
