import { User } from "../models/user";

export interface ChildDataAccessInterface {
  getChildParent(childId: number): Promise<User | undefined>;
}
