import { inject, injectable } from "inversify";
import { ChildDataAccessInterface } from "./child.interface";
import { User } from "../models/user";
import { Child } from "../models/child";
import { InterfaceErrorHandlerPlugin } from "../../plugins/error-handler.interface";

@injectable()
export class ChildDataAccess implements ChildDataAccessInterface {
  private _error: InterfaceErrorHandlerPlugin;

  constructor(
    @inject(nameof<InterfaceErrorHandlerPlugin>())
    errorFactory: (name: string) => InterfaceErrorHandlerPlugin
  ) {
    this._error = errorFactory(nameof<ChildDataAccess>());
  }

  async getChildParent(childId: number): Promise<User | undefined> {
    try {
      const child = await Child.findOne({
        where: { id: childId },
        include: [{ model: User, required: true }],
      });
      if (child && child.parent) {
        return child.parent;
      }
      return;
    } catch (error) {
      throw this._error.getFormattedError({
        status: 500,
        message: "Error while getting parent by childId",
        source: "intdb",
        errorData: {
          error,
          childId,
        },
      });
    }
  }
}
