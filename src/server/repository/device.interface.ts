import { Device } from "../models/device";

export interface DeviceDataAccessInterface {
  getByUserId(userId: number): Promise<Device[] | null>;
}
