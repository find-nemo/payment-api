import { Container } from "inversify";

import { PluginManager } from "./plugins.config";
import { InterfaceErrorHandlerPlugin } from "./plugins/error-handler.interface";
import { ErrorHandlerPlugin } from "./plugins/error-handler.plugin";
import { InterfaceRequestValidationMiddleware } from "./plugins/request-validation.interface";
import { RequestValidationMiddleware } from "./plugins/request-validation.plugin";
import { InterfaceSecretManagerPlugin } from "./plugins/secret-manager.interface";
import { SecretManagerPlugin } from "./plugins/secret-manager.plugin";
import { InterfaceSequelizePlugin } from "./plugins/sequelize.interface";
import { SequelizePlugin } from "./plugins/sequelize.plugin";
import { HealthController } from "./server/controllers/health.controller";
import { PaymentController } from "./server/controllers/payment.controller";
import { SwaggerController } from "./server/controllers/swagger.controller";
import { FirebaseTokenMiddleware } from "./server/middlewares/firebase-token.middleware";
import { SYMBOLS } from "./server/models/error-symbol";
import { PaymentDataAccess } from "./server/repository/payment.da";
import { PaymentDataAccessInterface } from "./server/repository/payment.interface";
import { UserDataAccess } from "./server/repository/user.da";
import { UserDataAccessInterface } from "./server/repository/user.interface";
import { PaymentServiceInterface } from "./server/services/payment.interface";
import { PaymentService } from "./server/services/payment.service";
import { UserServiceInterface } from "./server/services/user.interface";
import { UserService } from "./server/services/user.service";
import { CloudMessagingInterface } from "./server/services/cloud-messaging.interface";
import { CloudMessagingService } from "./server/services/cloud-messaging.service";
import { ChildDataAccessInterface } from "./server/repository/child.interface";
import { ChildDataAccess } from "./server/repository/child.da";
import { DeviceDataAccessInterface } from "./server/repository/device.interface";
import { DeviceDataAccess } from "./server/repository/device.da";

const appContainer = new Container();
appContainer
  .bind<PluginManager>(nameof<PluginManager>())
  .to(PluginManager)
  .inSingletonScope();
appContainer
  .bind<InterfaceErrorHandlerPlugin>(nameof<InterfaceErrorHandlerPlugin>())
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  .toFactory<InterfaceErrorHandlerPlugin>((): any => {
    return (name: string): ErrorHandlerPlugin => {
      name = name || "UNKNOWN";
      const service = appContainer.get<ErrorHandlerPlugin>(
        SYMBOLS.DiagnosticsInstance
      );
      service.setName(name);
      return service;
    };
  });
appContainer
  .bind<InterfaceErrorHandlerPlugin>(SYMBOLS.DiagnosticsInstance)
  .to(ErrorHandlerPlugin)
  .inTransientScope();
appContainer
  .bind<InterfaceRequestValidationMiddleware>(
    nameof<InterfaceRequestValidationMiddleware>()
  )
  .to(RequestValidationMiddleware)
  .inTransientScope();
appContainer
  .bind<SwaggerController>(nameof<SwaggerController>())
  .to(SwaggerController);
appContainer
  .bind<HealthController>(nameof<HealthController>())
  .to(HealthController);
appContainer
  .bind<FirebaseTokenMiddleware>(nameof<FirebaseTokenMiddleware>())
  .to(FirebaseTokenMiddleware);
appContainer
  .bind<InterfaceSecretManagerPlugin>(nameof<InterfaceSecretManagerPlugin>())
  .to(SecretManagerPlugin)
  .inSingletonScope();
appContainer
  .bind<InterfaceSequelizePlugin>(nameof<InterfaceSequelizePlugin>())
  .to(SequelizePlugin)
  .inSingletonScope();
appContainer
  .bind<PaymentDataAccessInterface>(nameof<PaymentDataAccessInterface>())
  .to(PaymentDataAccess)
  .inSingletonScope();
appContainer
  .bind<PaymentServiceInterface>(nameof<PaymentServiceInterface>())
  .to(PaymentService)
  .inSingletonScope();
appContainer
  .bind<PaymentController>(nameof<PaymentController>())
  .to(PaymentController);
appContainer
  .bind<UserDataAccessInterface>(nameof<UserDataAccessInterface>())
  .to(UserDataAccess)
  .inSingletonScope();
appContainer
  .bind<UserServiceInterface>(nameof<UserServiceInterface>())
  .to(UserService)
  .inSingletonScope();
appContainer
  .bind<CloudMessagingInterface>(nameof<CloudMessagingInterface>())
  .to(CloudMessagingService)
  .inSingletonScope();
appContainer
  .bind<ChildDataAccessInterface>(nameof<ChildDataAccessInterface>())
  .to(ChildDataAccess)
  .inSingletonScope();
appContainer
  .bind<DeviceDataAccessInterface>(nameof<DeviceDataAccessInterface>())
  .to(DeviceDataAccess)
  .inSingletonScope();
export { appContainer };
