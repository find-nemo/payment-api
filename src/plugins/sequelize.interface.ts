import { SequelizeOptions } from 'sequelize-typescript';
import { Sequelize } from 'sequelize/types';

export interface InterfaceSequelizePlugin {
  getSequelize(): Sequelize | undefined;
  makeConnection(options: SequelizeOptions): Promise<void>;
}
